package com.example.spring.web.learnspringweb;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
public class Controller {
    @Autowired
    private  BusinessLogic businessLogic;
    @GetMapping("/Add")
    public long  displaySum() {
        return businessLogic.calculateSum();

    }

}
