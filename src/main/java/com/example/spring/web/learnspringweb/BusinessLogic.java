package com.example.spring.web.learnspringweb;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class BusinessLogic {
    @Autowired
    private  DataService dataService;
    public long calculateSum() {
        List<Integer> data= dataService.retriveData();
        return data.stream().reduce(Integer ::sum).get();
    }
}
